package com.example.restapi.latihan9restapi.repository;

import com.example.restapi.latihan9restapi.entity.Rekening;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RekeningRepo extends JpaRepository<Rekening, Long> {
    @Query("select e from Rekening e where e.id = :id")
    public Rekening getById(@Param("id") Long id);

    @Query("select e from Rekening e")
    public List<Rekening> getAllRekening();
}
