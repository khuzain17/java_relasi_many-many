package com.example.restapi.latihan9restapi.entity;

import com.example.restapi.latihan9restapi.entity.Abstract.AbstractDate;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "rekening")
@Where(clause = "deleted_date is null")
public class Rekening extends AbstractDate implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nama_bank", length = 20)
    private String namaBank;

    @Column(name = "jenis", length = 20)
    private String jenis;

    @Column(name = "nomor", length = 25)
    private String nomor;

//    @JsonIgnore
    @ManyToOne(targetEntity = Employee.class, cascade = CascadeType.ALL)
    private Employee employee;
}
