package com.example.restapi.latihan9restapi.service;

import com.example.restapi.latihan9restapi.entity.EmployeeTraining;

import java.util.Map;

public interface EmployeeTrainingService {
    public Map insert(EmployeeTraining employeeTraining);

    public Map update(EmployeeTraining employeeTraining);

    public Map delete(Long idemployeeTraining);

    public Map getAll();
}
