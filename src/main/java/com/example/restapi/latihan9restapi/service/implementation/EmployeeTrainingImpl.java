package com.example.restapi.latihan9restapi.service.implementation;

import com.example.restapi.latihan9restapi.HelperRes.ResHelper;
import com.example.restapi.latihan9restapi.entity.Employee;
import com.example.restapi.latihan9restapi.entity.EmployeeTraining;
import com.example.restapi.latihan9restapi.entity.Training;
import com.example.restapi.latihan9restapi.repository.EmployeeRepo;
import com.example.restapi.latihan9restapi.repository.EmployeeTrainingRepo;
import com.example.restapi.latihan9restapi.repository.TrainingRepo;
import com.example.restapi.latihan9restapi.service.EmployeeTrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class EmployeeTrainingImpl implements EmployeeTrainingService {
    @Autowired
    public EmployeeTrainingRepo employeeTrainingRepo;

    @Autowired
    public EmployeeRepo employeeRepo;

    @Autowired
    public TrainingRepo trainingRepo;

    ResHelper respHelper;

    @Override
    public Map insert(EmployeeTraining employeeTraining) {
        Map map = new HashMap();
        try {
            Employee objKaryawan = employeeRepo.getById(employeeTraining.getEmployee().getId());
            if (objKaryawan == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Employee does not exist");
                return map;
            }

            Training objTraining = trainingRepo.getById(employeeTraining.getTraining().getId());
            if (objTraining == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Training does not exist");
                return map;
            }

            employeeTraining.setEmployee(objKaryawan);
            employeeTraining.setTraining(objTraining);

            EmployeeTraining resp = employeeTrainingRepo.save(employeeTraining);

            map.put("data", resp);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee created successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map update(EmployeeTraining employeeTraining) {
        Map map = new HashMap();
        try {
            EmployeeTraining obj = employeeTrainingRepo.getById(employeeTraining.getId());
            Employee karyawan = employeeRepo.getById(employeeTraining.getEmployee().getId());
            Training training = trainingRepo.getById(employeeTraining.getTraining().getId());

            if (employeeTraining.getUpdated_date() != null) {
                obj.setUpdated_date(employeeTraining.getUpdated_date());
            }
            obj.setEmployee(karyawan);
            obj.setTraining(training);

            EmployeeTraining result = employeeTrainingRepo.save(obj);

            map.put("data", result);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee updated successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }

    }

    @Override
    public Map delete(Long idemployeeTraining) {
        Map map = new HashMap();
        try {
            EmployeeTraining obj = employeeTrainingRepo.getById(idemployeeTraining);
            obj.setDeleted_date(new Date());

            EmployeeTraining result = employeeTrainingRepo.save(obj);
            map.put("data", result);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee deleted successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }

    }

    @Override
    public Map getAll() {
        Map map = new HashMap();
        List<EmployeeTraining> employees = new ArrayList<EmployeeTraining>();
        try {
            employees = employeeTrainingRepo.getAllEmployeeTraining();
            map.put("data", employees);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee get all successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }

    }
}
