package com.example.restapi.latihan9restapi.service.implementation;

import com.example.restapi.latihan9restapi.entity.Employee;
import com.example.restapi.latihan9restapi.entity.EmployeeDetail;
import com.example.restapi.latihan9restapi.repository.EmployeeDetailRepo;
import com.example.restapi.latihan9restapi.repository.EmployeeRepo;
import com.example.restapi.latihan9restapi.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

//Step-step:
//1. Tambahkan anotasi @Service dan @Transactional
//2. Jangan lupa menambahkan anotasi @Autowired sebagia DI
//3. definisikan Map map sebagai wadah menampung message, status, dan data
//4. lakukan save jika sudah memasukkan data
//5. map.put untuk memasukkan data
//6. dapat melakukan pengecekan null untuk mengetahui data kosong atau tidak

@Service
@Transactional
public class EmployeeImpl implements EmployeeService {

    @Autowired
    public EmployeeRepo repo;

    @Autowired
    public EmployeeDetailRepo employeeDetailRepo;

    @Override
    public Map insert(Employee employee) {
        Map map = new HashMap();
        try {
            EmployeeDetail employeeDetail = employeeDetailRepo.save(employee.getEmployeeDetail());
            Employee obj = repo.save(employee);

            employeeDetail.setEmployee(obj);
            employeeDetailRepo.save(employeeDetail);

            map.put("data", obj);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee created successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map update(Employee employee) {
        Map map = new HashMap();
        try {
            Employee emp = repo.getById(employee.getId());


//            EmployeeDetail employeeDetail = employeeDetailRepo.save(employee.getEmployeeDetail());
//            Employee obj = repo.save(employee);

//            employeeDetail.setEmployee(obj);
//            employeeDetailRepo.save(employeeDetail);


            if (emp == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Employee does not exist");
                return map;
            }

            emp.setName(employee.getName());
            emp.setSex(employee.getSex());
            emp.setBirthDate(employee.getBirthDate());
            emp.setAddress(employee.getAddress());
            emp.setStatus(employee.getStatus());
            repo.save(emp);

            map.put("data", emp);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee updated successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map delete(Long employeeId) {
        Map map = new HashMap();
        try {
            Employee emp = repo.getById(employeeId);
            EmployeeDetail employeeDetail= employeeDetailRepo.getById(emp.getEmployeeDetail().getId());

            if (emp == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Employee does not exist");
                return map;
            }
            emp.setDeleted_date(new Date());
            repo.save(emp);

            employeeDetail.setDeleted_date(new Date());
            employeeDetailRepo.save(employeeDetail);

            map.put("empdetail", employeeDetail);
            map.put("data", emp);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee deleted successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getAll() {
        List<Employee> employees = new ArrayList<Employee>();
        Map map = new HashMap();
        try {
            employees = repo.getAllEmployee();
            map.put("data", employees);
            map.put("statusCode", "200");
            map.put("statusMessage", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getByStatus(int status) {
        List<Employee> employees = new ArrayList<Employee>();
        Map map = new HashMap();
        try {
            employees = repo.getByStatus(status);
            map.put("data", employees);
            map.put("statusCode", "200");
            map.put("statusMessage", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }
}
